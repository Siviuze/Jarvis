import argparse
import shlex        # split avec support des sous chaines


def reqfile_parser(file_path):

    # liste des entrées à remplir et à retourner
    doc_reqs  = []
    reqs      = []

    # extraction des données du fichier de conf
    with open(file_path) as f:

        # variable pour traiter les entrées sur plusieurs lignes
        multiline = False

        for line in iter(f.readline, ''):
            if line[:1] == '#':
                # filter comments
                continue

            # filtrage des caractères inutiles
            line = line.replace('\n', '')  # suppression des retours à la ligne
            line = line.replace('  ', '')  # suppression des espaces en trop
            data = line.replace('\\', '')  # suppression des espaces en trop

            if not line:
                # ligne vide
                continue

            if multiline:
                doc_reqs[-1] += data
            else:
                doc_reqs.append(data)

            # détection des multilignes
            if line.endswith('\\'):
                multiline = True
            else:
                multiline = False

    # traitement des entrées du fichier
    parser = argparse.ArgumentParser()
    parser.add_argument('document')
    parser.add_argument('type')
    parser.add_argument('-path')
    parser.add_argument('-req')
    parser.add_argument('-ref')
    parser.add_argument('-nocov', action='store_true')
    parser.add_argument('-sort')
    parser.add_argument('-start-after')
    parser.add_argument('-stop-after')

    for e in doc_reqs:
        if not e.startswith('document'):
            # mauvaise donnée
            print('Oops')
            continue

        reqs.append(vars(parser.parse_args(shlex.split(e))))

    return reqs


reqs = reqfile_parser('conf.req')


def write_reqfile(file_path, reqs):
    with open(file_path, 'w') as f:
        for e in reqs:
            entry = 'document {} -path "{}" -req {}'.format(e['type'], e['path'], e['req'])
            if e.get('ref', None):
                entry += ' -ref {}'.format(e['ref'])
            if e.get('start_after', None):
                entry += ' -start-after "{}"'.format(e['start_after'])
            if e.get('stop_after', None):
                entry += ' -stop-after "{}"'.format(e['stop_after'])
            if e.get('nocov', None):
                entry += ' -nocov'
            if e.get('sort', None):
                entry += ' -sort {}'.format(e['sort'])

            f.write(entry)
            f.write('\n')

# analyse des entrées
#for e in reqs:
#    print('{:<10} {:<25} {:<25} {:<10} {:<10}'.format(e['type'], e['path'], e['req'], str(e['ref']), str(e['nocov'])))

# ajout d'un entrée
# reqs.append(
#     {
#         'document': 'document',
#         'type':     'TSEP',
#         'path':     'TSEP/RD11_0168_Motherboard.pdf',
#         'req':      'TSEP_[-a-zA-Z0-9_]*',
#     }
# )
#
# write_reqfile('new_conf.req', reqs)
