import tkinter as tk
import pygubu

# optionnals modules of tkinter that must be import separately
from tkinter import messagebox
from tkinter import filedialog

import reqfile


class Application(pygubu.TkApplication):  # inherit from pygubu
    def _create_ui(self):
        # 1: Create a builder
        self.builder = pygubu.Builder()

        # 2: Load an ui file
        self.builder.add_from_file('mainwindow.ui')

        # 3: Create the widget using a master as parent
        self.builder.get_object('mainwindow', self.master)

        # Set main menu
        self.mainmenu = self.builder.get_object('mainmenu', self.master)
        self.set_menu(self.mainmenu)

        # doc_tree
        self._create_trees()

        # 4: Connect callbacks
        self.builder.connect_callbacks(self)

        # + Configure layout of the master. Set master resizable:
        self.set_resizable()


    def on_mfile_quit_clicked(self):
        self.master.quit()

    def on_mfile_open_clicked(self):
        path = filedialog.askopenfilename(defaultextension='req')
        self.doc_list = reqfile.reqfile_parser(path)
        for e in self.doc_list:
            self.doc_tree.insert('', 'end', text=e['type'], values=(e['path'], e['type']))
            self.template_tree.insert('', 'end', text=e['type'], values=(e['type'], e['req'], e['ref'], e['start_after'], e['stop_after']))


    def on_mfile_save_clicked(self):
        pass

    def _create_trees(self):

        # tab doc
        self.doc_tree = self.builder.get_object('doc_tree')
        self.doc_tree['columns'] = ('name', 'template', 'actions')

        self.doc_tree.heading('name', text='Name', anchor='w')
        self.doc_tree.column('name', anchor="w")

        self.doc_tree.heading('template', text='Req Rule')
        self.doc_tree.column('template', anchor='center', width=100)

        self.doc_tree.heading('actions', text='Actions')
        self.doc_tree.column('actions', anchor='center', width=100)

        # tab template
        self.template_tree = self.builder.get_object('template_tree')
        self.template_tree['columns'] = ('type', 'reqrule', 'refrule', 'start', 'stop', 'actions')

        self.template_tree.heading('type', text='Type', anchor='w')
        self.template_tree.column('type', anchor="w")

        self.template_tree.heading('reqrule', text='Req Rule')
        self.template_tree.column('reqrule', anchor='center', width=100)

        self.template_tree.heading('refrule', text='Ref Rule')
        self.template_tree.column('refrule', anchor='center', width=100)

        self.template_tree.heading('start', text='Start after')
        self.template_tree.column('start', anchor='center', width=100)

        self.template_tree.heading('stop', text='Stop after')
        self.template_tree.column('stop', anchor='center', width=100)

        self.template_tree.heading('actions', text='Actions')
        self.template_tree.column('actions', anchor='center', width=100)


if __name__ == '__main__':
    root = tk.Tk()
    root.withdraw()
    app = Application(root)
    root.deiconify()
    app.run()
