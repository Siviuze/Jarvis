Welcome to Jarvis!
============================================

'Jarvis' is a project that aim to set up an IHM onto the ReqFlow project (http://goeb.github.io/reqflow/).



Installation
====

Jarvis requires Python >= 3.5 (Tested only in Python 3.5.2)


### Packages :
  - Pygubu (https://github.com/alejandroautalan/pygubu)
  - Python regex from Barnett (https://bitbucket.org/mrabarnett/mrab-regex)
  - pdftotext (xpdf)
  - python-docx


  


